class SetOfStacks:
    def __init__(self, capacity=3):
        if capacity < 1:
            print("capacity must be at least 1")

        self.capacity = capacity
        self.stacks = [[]]

    def push(self, val):
        if len(self.stacks[-1]) == self.capacity:
            self.stacks.append([])

        self.stacks[-1].append(val)

    def pop(self):
        if len(self.stacks[-1]) == 0:
            if len(self.stacks) == 1:
                print("pop from empty stack")
            else:
                self.stacks.pop()

        return self.stacks[-1].pop()


if __name__ == "__main__":
    s = SetOfStacks()
    s.push(1)
    s.push(2)
    s.push(3)
    s.push(4)
    s.push(5)
    s.push(6)
    print("Stacks: ", s.stacks)
    s.pop()
    s.pop()
    print("Stacks: ", s.stacks)