def smallest_subarray(arr, n):
    # end = n - 1
    # start = 0
    for start in range(0, n - 1):
        # print("start",start)
        if arr[start] > arr[start + 1]:
            break
    # print(start,n)
    if start + 1 == n - 1:
        # print(start)
        return [-1, -1]

    end = n - 1

    for e in range(end, -1, -1):
        if arr[e] < arr[e - 1]:
            break
        end -= 1

    max = arr[start]
    min = arr[start]
    for i in range(start + 1, end + 1):
        if arr[i] > max:
            max = arr[i]
        if arr[i] < min:
            min = arr[i]

    for i in range(start):
        if arr[i] > min:
            start = i
            break

    i = n - 1
    while i >= end + 1:
        if arr[i] < max:
            end = i
            break
        i -= 1

    return [start, end]


if __name__ == '__main__':
    arr = [[1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19],
               [4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 51, 7],
               [1, 2, 3, 4, 5]]
    for a in arr:
        print(smallest_subarray(a, len(a)))