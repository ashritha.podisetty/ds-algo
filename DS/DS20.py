class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def get_paths_sum(root, val, res):
    if root is None:
        return 0

    val = (val + root.data)

    if root.left is None and root.right is None:
        res.append(val)
        return val
    get_paths_sum(root.left, val, res)
    get_paths_sum(root.right, val, res)
    return res


def path_sum(root):
    res = []
    return get_paths_sum(root, 0, res)


if __name__ == '__main__':
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(3)
    root.left.right = Node(4)
    root.right.left = Node(4)
    root.right.right = Node(6)
    root.left.left.right = Node(7)
    root.right.right.left = Node(8)
    print(path_sum(root))