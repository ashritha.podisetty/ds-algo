class newNode:
    def __init__(self, data):
        self.data = data
        self.right = self.left = None

def KthLargestTraversal(root, k):
    curr = root
    Klargest = None

  
    count = 0

    while (curr != None):
        
        
        if (curr.right == None):

            count += 1
            if (count == k):
                Klargest = curr

            curr = curr.left

        else:

            succ = curr.right

            while (succ.left != None and
                succ.left != curr):
                succ = succ.left

            if (succ.left == None):

                succ.left = curr
                curr = curr.right

            else:

                succ.left = None
                count += 1
                if (count == k):
                    Klargest = curr
                curr = curr.left

    return Klargest

if __name__ == '__main__':

    root = newNode(15)
    root.left = newNode(5)
    root.right = newNode(20)
    root.left.left = newNode(2)
    root.left.right = newNode(5)
    root.right.left = newNode(17)
    root.right.right = newNode(27)

    print("Finding K-th largest Node in BST : ",
        KthLargestUsingMorrisTraversal(root, 3).data)

