class Graph:

    def __init__(self, row, col, graph):
        self.ROW = row
        self.COL = col
        self.graph = graph

    def mark_edge_points(self, i, j):
        if i < 0 or i >= self.ROW or j < 0 or j >= self.COL or self.graph[i][j] == 'x' or self.graph[i][j] != 1:
            return
        self.graph[i][j] = -1
        if i == 0 or j == 0 or i == self.ROW - 1 or j == self.COL - 1:
            if self.graph[i][j] == -1:
                self.graph[i][j] = 'x'
        self.mark_edge_points(i, j - 1)
        self.mark_edge_points(i, j + 1)
        self.mark_edge_points(i + 1, j)
        self.mark_edge_points(i - 1, j)

    def mark_edges(self, i, j):
        if i < 0 or i >= i >= self.ROW or j < 0 or j >= self.COL or self.graph[i][j] == 0 or self.graph[i][j] == "x1":
            return
        self.graph[i][j] = "x1"

        self.mark_edges(i, j - 1)
        self.mark_edges(i, j + 1)
        self.mark_edges(i + 1, j)
        self.mark_edges(i - 1, j)

    def remove_islands(self):
        for i in range(self.ROW):
            for j in range(self.COL):
                if (i != 0 and j != 0 and i != self.ROW - 1 and j != self.COL - 1) and self.graph[i][j] == 1:
                    self.mark_edge_points(i, j)
        for i in range(self.ROW):
            for j in range(self.COL):
                if self.graph[i][j] == 'x':
                    self.mark_edges(i, j)
        for i in range(self.ROW):
            for j in range(self.COL):
                if self.graph[i][j] == -1:
                    self.graph[i][j] = 0
                if self.graph[i][j] == "x1":
                    self.graph[i][j] = 1


if __name__ == "__main__":
    arr = [
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 1],
        [0, 0, 1, 0, 1, 0],
        [1, 1, 0, 0, 1, 0],
        [1, 0, 1, 1, 0, 0],
        [1, 0, 0, 0, 0, 1]
    ]

    g = Graph(len(arr), len(arr[0]), arr)

    print(g.remove_islands())
    for i in range(len(arr)):
        print(g.graph[i])