class fuelRestore:
    def __init__(self, petrol, distance):
        self.petrol = petrol
        self.distance = distance


def get_starting_city(arr, n):
    start = 0
    for i in range(n):
        if arr[i].petrol >= arr[i].distance:
            start = i
            break

    curr_petrol = 0
    for i in range(start, n):
        curr_petrol += (arr[i].petrol - arr[i].distance)
        if (curr_petrol < 0):
            i += 1
            while (i < n):
                if (arr[i].petrol >= arr[i].distance):
                    start = i

                    curr_petrol = 0
                    break
                i += 1

        else:
            i += 1

    if (curr_petrol < 0):
        return -1

    for i in range(start):
        curr_petrol += (arr[i].petrol - arr[i].distance)
        if (curr_petrol < 0):
            return -1
    return start


if __name__ == "__main__":
    distance = [
        [5, 25, 15, 10, 15],
        [30, 40, 10, 10, 17, 13, 50, 30, 10, 40]
    ]
    fuels = [
        [1, 2, 1, 0, 3],
        [1, 2, 0, 1, 1, 0, 3, 1, 0, 1]
    ]
    mileage = [10, 25]
    for i in range(len(distance)):
        arr = []
        for d in range(len(distance[i])):
            arr.append(fuelRestore(fuels[i][d] * mileage[i], distance[i][d]))
        start = get_starting_city(arr, len(arr))
        if start == -1:
            print("No solution can be found.")
        else:
            print("Start at = {}".format(start))