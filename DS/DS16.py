def minimum_wait_time(a, n):
    a.sort()
    previous = 0
    total_wait = 0
    for i in range(1,n):
        previous = previous + a[i-1]
        total_wait = total_wait + previous
    return total_wait

if __name__ == '__main__':
    arr = [
        [3, 2, 1, 2, 6],
        [1, 2, 4, 5, 2, 1]
    ]
    for a in arr:
        print(f"Total Minimum time:{minimum_wait_time(a, len(a))}")