def get_largest_range(arr):
    arr.sort()
    r1 = 0
    r2 = 0
    cur_max = 0
    tmp_srt = arr[0]
    count = 0
    for i in range(len(arr) - 1):
        if abs(arr[i] - arr[i + 1]) == 1 or abs(arr[i] - arr[i + 1]) == 0:
            count += 1
        if abs(arr[i] - arr[i + 1]) > 1:
            if count + 1 > cur_max:
                cur_max = count + 1
                count = 0
                r1 = tmp_srt
                r2 = arr[i]
                tmp_srt = arr[i + 1]
        if i + 1 == len(arr) - 1:
            if count + 1 > cur_max:
                cur_max = count + 1
                count = 0
                r1 = tmp_srt
                if abs(arr[i] - arr[i + 1]) > 1:
                    r2 = arr[i]
                else:
                    r2 = arr[i + 1]
                tmp_srt = arr[i + 1]
    return [r1, r2]


if __name__ == "__main__":
    arr = [
        [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6],
        [19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14]
    ]
    for a in arr:
        print(f"largest range of integers contained in the array {a} is: {get_largest_range(a)}")