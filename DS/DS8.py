def get_next_greater_elements(arr):
    s = []
    l = len(arr)
    res = [-1 for i in range(l)]
    s.append(0)
    max_num = 0
    for i in range(1, 2 * l):
        if arr[i % l] > arr[s[len(s) - 1]]:
            max_num = max(arr[i % l], max_num)
            while len(s):
                if arr[s[len(s) - 1]] < max_num:
                    res[s[len(s) - 1]] = arr[i % l]
                    s.pop()
                else:
                    s.pop()
        s.append(i % l)
    return res

if __name__ == '__main__':
    arr = [[2, 5, -3, -4, 6, 7, 2],
           [7, 6, 5, 4, 3, 2, 1]]
    for a in arr:
        print(get_next_greater_elements(a))