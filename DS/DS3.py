def swap(arr, i, j):
    temp = arr[i]
    arr[i] = arr[j]
    arr[j] = temp

def Partition(arr):
    start = mid = 0
    pivot = 1
    end = len(arr) - 1
 
    while mid <= end:
        if arr[mid] < pivot:     
            swap(arr, start, mid)
            start = start + 1
            mid = mid + 1
        elif arr[mid] > pivot:    
            swap(arr, mid, end)
            end = end - 1
        else:                   
            mid = mid + 1
    return arr
arr=[0, 1, 2, 0, 1, 2] 
a=Partition(arr)
print(a)