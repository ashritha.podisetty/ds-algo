def kadanes_algo(a):
    max_int = a[0]
    prev_sum = 0
    for n in a:
        prev_sum = max(prev_sum + n, n)
        max_int = max(max_int, prev_sum)
    return max_int


if __name__ == '__main__':
    arr = [
        [-2, -3, 4, -1, -2, 1, 5, -3],
        [-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4, -7]
        ]
    for a in arr:
        print(f"Maxsum: {kadanes_algo(a)}")