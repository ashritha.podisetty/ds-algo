
from collections import deque
 
 
def isValid(i, j, M, N):
    return (0 <= i < M) and (0 <= j < N)
 
 
row = [-1, 0, 0, 1]
col = [0, -1, 1, 0]
 
 
def hasNegative(mat):
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            if mat[i][j] < 0:
                return True
    return False
 
def findMinPasses(mat):
 
    # base case
    if not mat or not len(mat):
        return 0
 
    (M, N) = (len(mat), len(mat[0]))
 
    Q = deque()
 
    for i in range(M):
        for j in range(N):
            if mat[i][j] > 0:
                Q.append((i, j))
 
    passes = 0
 
    while Q:

        q = Q.copy()
        Q.clear()
 
 
        while q:
 
            x, y = q.popleft()
 
            for k in range(len(row)):
                if isValid(x + row[k], y + col[k], M, N) and \
                        mat[x + row[k]][y + col[k]] < 0:
                    mat[x + row[k]][y + col[k]] = -1 * mat[x + row[k]][y + col[k]]
 
                    Q.append((x + row[k], y + col[k]))
 
        passes = passes + 1
 
    return -1 if hasNegative(mat) else (passes - 1)
 
 
if __name__ == '__main__':
 
    mat = [
 [0, -1, -3, 2, 0],
 [1, -2, -5, -1, -3],
 [3, 0, 0, -4, -1]]

    passes = findMinPasses(mat)
    if passes != -1:
        print("No of passes required is", passes)
    else:
        print("Invalid Input")