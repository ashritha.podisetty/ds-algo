class Graph:

    def __init__(self, row, col, graph):
        self.ROW = row
        self.COL = col
        self.graph = graph

    def DFS(self, i, j):
        if i < 0 or i >= len(self.graph) or j < 0 or j >= len(self.graph[0]) or self.graph[i][j] != 1:
            return 0
        self.graph[i][j] = -1

        r = self.DFS(i, j - 1) + self.DFS(i, j + 1) + self.DFS(i + 1, j) + self.DFS(i + 1, j + 1) + 1
        return r

    def lengthOfIslands(self):

        res = []
        for i in range(self.ROW):
            for j in range(self.COL):

                if self.graph[i][j] == 1:
                    res.append(self.DFS(i, j))
        return res


if __name__ == '__main__':
    arr = [
        [1, 0, 0, 1, 0],
        [1, 0, 1, 0, 0],
        [0, 0, 1, 0, 1],
        [1, 0, 1, 0, 1],
        [1, 0, 1, 1, 0]
    ]

    g = Graph(len(arr), len(arr[0]), arr)

    print(g.lengthOfIslands())