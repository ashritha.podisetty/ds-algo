def get_min_steps(arr):
    jumps=[float('inf') for _ in range(len(arr))]
    jumps[0]=0
    
    for i in range(1,len(arr)):
        for j in range(0,i):
            if arr[j]+j >=i:
                jumps[i] = min(jumps[i], jumps[j]+ 1)

    print(jumps)
    return jumps[-1]

min_jumps = get_min_steps([3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3])
print(min_jumps)