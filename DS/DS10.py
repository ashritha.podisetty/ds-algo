def max_sum(arr):
    st1 = arr[-1]
    st2 = arr[-2] if arr[-2] > arr[-1] else arr[-1]
    max_sum = max(st1, st2)
    for i in range(len(arr) - 3, -1, -1):
        max1 = st1
        max2 = st2 + arr[i]
        st1 = max2
        st2 = max1
        max_sum = max(st1, st2)

    return max_sum

if __name__ == '__main__':
    arr = [[7, 10, 12, 7, 9, 14],
           [75, 105, 120, 75, 90, 135]]
    for a in arr:
        print(max_sum(a))