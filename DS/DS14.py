class Node(object):
    def __init__(self, data):
        self.left = None
        self.right = None
        self.val = data


def level_sum(root, arr, level):
    if root is None:
        return
    if (root != None):
        level_sum(root.left, arr, (level + 1))
        arr[level] += root.val
        level_sum(root.right, arr, (level + 1))


def height(root):
    if root is None:
        return 0
    lheight = height(root.left)
    rheight = height(root.right)
    return (max(lheight, rheight) + 1)


def Insert(root):
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(3)
    root.left.right = Node(4)
    root.right.left = Node(5)
    root.right.right = Node(6)
    root.left.left.right = Node(7)
    root.right.right.left = Node(8)
    return (root)


if __name__ == "__main__":
    root = None
    root = Insert(root)
    h = height(root)
    arr = [0] * h
    level_sum(root, arr, 0)
    for i in arr:
        print(i, end=', ')