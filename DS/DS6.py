def solution(arr):
    res = []
    sum_of_arr = 1
    for i in arr:
        sum_of_arr *= i
    for i in arr:
        res.append(sum_of_arr // i)
    return res


if __name__ == '__main__':
    arr = [5, 1, 4, 2]
    print(solution(arr))